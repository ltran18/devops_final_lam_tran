# install OS layer to your dockerfile
FROM ubuntu:latest

# Label your dockerfile
LABEL authors="Lam ltran000@citymail.cuny.edu"

# install node v8 to run environment
RUN apt-get update
RUN apt-get install curl wget -y
RUN curl --silent --location https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get -y install nodejs wget

# install yarn in global mode
RUN npm install -g yarn

# set app folder env variables - as per standard it should follow /liveperson/code/name_of_project
ENV LP_HOME="/liveperson"
ENV APP_CODE="${LP_HOME}/code/agent_widget"

# create folder where application will be running
RUN mkdir -p ${APP_CODE}/

# install external dependencies
COPY package*.json .${APP_CODE}/

# install your global dependencies
RUN cd ${APP_CODE}/ && \
    yarn install && \
    yarn cache clean 

# copy files that are required for the app to work - modify the lines accordingly
# change working dir
WORKDIR ${APP_CODE}

# start server and provide port that you will expose
EXPOSE 3000
CMD yarn start ${APP_CODE}